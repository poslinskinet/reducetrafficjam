class AddRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table(:routes) do |t|
      t.string :from, null: false, default: ''
      t.string :to, null: false, default: ''
      t.datetime :check_at_start, null: false
      t.datetime :check_at_return

      t.timestamps
    end

    add_reference :routes, :user, foreign_key: true
  end
end
