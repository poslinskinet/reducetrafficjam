Rails.application.routes.draw do
  root to: 'my_routes#index'

  resources :my_routes, only: :index
  resources :results, only: :show
end
