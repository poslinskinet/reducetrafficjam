require 'net/http'
require 'json'
require 'time'

class DataFetcher
  API_KEY = "AIzaSyC1GL5TmZ7wdgnWVJ4aRfM8OIXE6olXLXM"
  URL_BASE = "https://maps.googleapis.com/maps/api/distancematrix/json"

  def get_data(from, to, time)
    uri = URI("#{URL_BASE}?origins=#{from}&destinations=#{to}&key=#{API_KEY}&departure_time=#{time}")
    response = Net::HTTP.get(uri)
    JSON.parse(response)["rows"].first["elements"].first["duration_in_traffic"]["value"]
  end
end

def result(calculation_time)
  DataFetcher.new.get_data("Bielsko-Biala, Modrzewiowa 10", "Paris", calculation_time)
end

def run_loop(string_time, interval = 300)
  puts "======"
  time = Time.parse(string_time).to_i

  base_time = result(time)

  (-5..5).each do |time_distance|
    calculation_time = time + time_distance * interval
    diff = base_time - result(calculation_time)
    sufix = diff > 0 ? "faster" : "slower"
    year = diff * 4
    puts "Travel time: #{Time.at(calculation_time)}: #{diff} seconds #{sufix} (#{year} minutes per year)"
  end
end

# run_loop("2016-10-25 07:20:00") # 07:20
run_loop("2016-10-25 19:50:00") # 15:40
