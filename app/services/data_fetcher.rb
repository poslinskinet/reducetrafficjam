class DataFetcher
  # AIzaSyC1GL5TmZ7wdgnWVJ4aRfM8OIXE6olXLXM
  API_KEYS = %w(AIzaSyB6pZhz8KP0Q9oZ9NGbA5dMY4qegZgQRIo AIzaSyCjU8rZsEFrYYqmGFRsxRbdSutOWyvoE20 AIzaSyDhX7IauYqrgMKmGrb5GegOhu2xNRfa-14)
  URL_BASE = "https://maps.googleapis.com/maps/api/distancematrix/json"

  def get_data(from, to, time)
    uri = URI(URI.encode("#{URL_BASE}?origins=#{from}&destinations=#{to}&key=#{API_KEYS.sample}&departure_time=#{time}"))
    puts uri
    response = Net::HTTP.get(uri)
    JSON.parse(response)["rows"].first["elements"].first["duration_in_traffic"]["value"]
  end
end
