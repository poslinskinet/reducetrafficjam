class MyRoutesController < ApplicationController
  include TimeHelper

  INTERVAL = 300 # seconds
  CHECKS = 5
  DAYS = %w(Monday Tuesday Wednesday Thursday Friday)

  before_action :setup_params

  expose(:route) { MyRoute.new }
  expose(:results) { {} }
  expose(:duration) { get_time(my_params[:base_calculation_duration],
    my_params[:base_starting_time], my_params[:calculation_time]) }

  def index
    route.from = my_params.try(:[], :from)
    route.to = my_params.try(:[], :to)
    route.start_time = my_params.try(:[], :start_time)
  end

  private

  def setup_params
    @from = my_params.try(:[], :from)
    @to = my_params.try(:[], :to)
    DAYS.map do |day|
      results[day] ||= {}
      if my_params.try(:[], :start_time)
        base_starting_time = time_for_day(day, my_params[:start_time])
        base_calculation_duration = result(base_starting_time)
        (-CHECKS..CHECKS).map do |time_distance|
          results[day][time_distance] ||= {
            calculation_time: base_starting_time + time_distance * INTERVAL,
            base_calculation_duration: base_calculation_duration,
            base_starting_time: base_starting_time
          }
        end
      end
    end
  end

  def my_params
    params.try(:[], :my_route).try(:permit, [:from, :to, :start_time])
  end
end
