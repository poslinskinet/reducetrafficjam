class ResultsController < ApplicationController
  include TimeHelper

  before_action :setup_params

  expose(:duration) { get_time(*get_time_params) }

  private

  def get_time_params
    [my_params[:base_calculation_duration], my_params[:base_starting_time], my_params[:calculation_time]]
  end

  def setup_params
    @from = my_params[:from]
    @to = my_params[:to]
  end

  def my_params
    params.permit(:from, :to, :calculation_time, :base_calculation_duration, :base_starting_time)
  end
end
