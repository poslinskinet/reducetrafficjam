module TimeHelper
  def time_for_day(day, start_time)
    Time.parse("#{date_of_next(day)} #{start_time}").to_i
  end

  def result(calculation_time)
    DataFetcher.new.get_data(@from, @to, calculation_time)
  end

  def get_time(base_calculation_duration, base_time, calculation_time)
    duration = result(calculation_time)
    {
      calculation_time: calculation_time.try(:to_i),
      diff: base_calculation_duration.to_i - result(calculation_time),
      duration: duration
    }
  end

  def sufix(diff)
    diff > 0 ? "faster" : "slower"
  end

  private

  def date_of_next(day)
    date  = Date.parse(day)
    delta = date > Date.today ? 0 : 7
    date + delta
  end
end
